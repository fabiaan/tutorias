export default{
  async updateTutoring(Id, body, token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/${Id}`
    try {
      const res = await fetch(API, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
        body: JSON.stringify(body)
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async createTutoring(body, token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring`
    try {
      const res = await fetch(API, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
        body: JSON.stringify(body)
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async enviadasPendientes(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/pending`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async enviadasAceptadas(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/acepted`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async enviadasRechazadas(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/canceled`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async recibidasPendientesProfesor(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/t/received/pending`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async recibidasCanceladasProfesor(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/t/received/canceled`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async recibidasAceptadasProfesor(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/t/received/acepted`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async recibidasAceptadasEstudiantes(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/e/received/acepted`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async recibidasPendientesEstudiantes(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/e/received/pending`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  },
  async recibidasCanceladasEstudiantes(token){
    const URL = 'http://13.85.41.121:8080'
    const API = `${URL}/v1/api/tutoring/e/received/canceled`
    try {
      const res = await fetch(API, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
      })
      const resJSON = await res.json()
      return resJSON
    } catch (e) {
      console.log(e)
    }
  }
}