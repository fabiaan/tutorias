import Vue from 'vue'
import Vuex from 'vuex'
import Swal from 'sweetalert2'
import createPersistedState from 'vuex-persistedstate'
import router from '@/router'

import auth from '@/logic/auth'
import subject from '@/logic/subject'
import tutoring from '@/logic/tutoring'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
    user: null
  },
  plugins: [createPersistedState()],
  getters: {
    getToken: state =>{
      return state.token
    }
  },
  mutations: {
    setToken(state, payload){
      state.token = payload
    },
    setUser(state, payload){
      state.user = payload
    }
  },
  actions: {
    async login({commit}, user){
      const response = await auth.login(user)
      commit('setToken', response.token)
      commit('setUser', response.user)
      // almacenar token
      localStorage.setItem('token', response.token)
      // redirigir
      const ruta = await auth.verifyRol(response.user.role)
      router.push(ruta)
    },
    readToken({commit}){
      if(localStorage.getItem('token')){
        commit('setToken', localStorage.getItem('token'))
      }else{
        commit('setToken', null)
      }
    },
    async getClass({commit}, token){
      return await subject.getClass(token)
    },
    async getStudents({commit}, id){
      return await subject.getStudents(id)
    },
    async getSubjectsTeacher({commit}, token){
      return await subject.getSubjectsTeacher(token)
    },
    // async createTutoring({commit}, {body, token}){
    //   const response = await tutoring.createTutoring(body, token)
    //   router.push({name: 'Sended'})
    //   return response
    // },
    async createTutoring({commit}, {body, token}){
      const response = await tutoring.createTutoring(body, token)
      Swal.fire({
        title: 'Tutoría creada correctamente',
        icon: 'success',
        html:
        `Autor: <b>${response.autor.name} ${response.autor.lastname}</b>` + '<br>' +
        `Fecha: <b>${response.date.substring(0,10)}</b>` + '<br>' +
        `Hora: <b>${response.date.substring(11,16)}</b>` + '<br>' +
        `Materia: <b>${response.subject.name}</b>` + '<br>' +
        `Paralelo: <b>${response.subject.class}</b>` + '<br>' +
        `Título: <b>${response.topic}</b>` + '<br>' +
        `Descripción: <b>${response.description}</b>`,
        showConfirmButton: false,
        showCloseButton: true
      })
      router.push({name: 'Sended'})
      return response
    },
    async createTutoringTeacher({commit}, {body, token}){
      const response = await tutoring.createTutoring(body, token)
      Swal.fire({
        title: 'Tutoría creada correctamente',
        icon: 'success',
        html:
        `Autor: <b>${response.autor.name} ${response.autor.lastname}</b>` + '<br>' +
        `Fecha: <b>${response.date.substring(0,10)}</b>` + '<br>' +
        `Hora: <b>${response.date.substring(11,16)}</b>` + '<br>' +
        `Materia: <b>${response.subject.name}</b>` + '<br>' +
        `Paralelo: <b>${response.subject.class}</b>` + '<br>' +
        `Título: <b>${response.topic}</b>` + '<br>' +
        `Descripción: <b>${response.description}</b>`,
        showConfirmButton: false,
        showCloseButton: true
      })
      router.push({name: 'TeacherSended'})
      return response
    },
    async enviadasPendientes({commit}, token){
      return await tutoring.enviadasPendientes(token)
    },
    async enviadasAceptadas({commit}, token){
      return await tutoring.enviadasAceptadas(token)
    },
    async enviadasRechazadas({commit}, token){
      return await tutoring.enviadasRechazadas(token)
    },
    async recibidasPendientesProfesor({commit}, token){
      return await tutoring.recibidasPendientesProfesor(token)
    },
    async updateTutoring({commit}, {Id, body, token}){
      const response = await tutoring.updateTutoring(Id, body, token)
      location.reload()
      return response
    },
    async recibidasCanceladasProfesor({commit}, token){
      return await tutoring.recibidasCanceladasProfesor(token)
    },
    async recibidasAceptadasProfesor({commit}, token){
      return await tutoring.recibidasAceptadasProfesor(token)
    },
    async recibidasAceptadasEstudiantes({commit}, token){
      return await tutoring.recibidasAceptadasEstudiantes(token)
    },
    async recibidasCanceladasEstudiantes({commit}, token){
      return await tutoring.recibidasCanceladasEstudiantes(token)
    },
    async recibidasPendientesEstudiantes({commit}, token){
      return await tutoring.recibidasPendientesEstudiantes(token)
    },
    cerrarSesion({commit}){
      localStorage.removeItem('token')
      commit('setToken', null)
      commit('setUser', null)
      router.push('/')
    },
  },
  modules: {
  }
})
